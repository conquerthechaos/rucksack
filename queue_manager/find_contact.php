<?php

	include_once('Infusionsoft/isdk.php');
    $isdk = new iSDK();

	$isdk->cfgCon("m148");

	$contact_id = $_REQUEST['contactid'];

	$returnFields = array(
		'Id',
		'Email',
		'FirstName',
		'LastName',
		'Company',
		'Phone1',
		'Phone2',
		'Address2Street1',
		'Address2Street2'
	);
	$query   = array('Id' => $contact_id);
	$contact = $isdk->dsQuery("Contact",100,0,$query,$returnFields);

	if($contact){

		print json_encode($contact[0]);

	}else{

		print "no";

	}

?>