$(function() {

	var queue_data = {};

	// $(".show_queue").on("click", function(){

	// 	console.log(queue_data);

	// })

	// $(document.body).on("click", ".delete_action", function(){

	// 	var queue_id = $(this).attr("queue_id");
	// 	console.log(queue_id)

	// })

	function sleep(sec){

		var start = new Date().getTime();
		var mili  = sec * 1000;
		for(var i = 0;i<1e7;i++){

			if((new Date().getTime() - start) > mili){

				break;

			}

		}

	}

	function clear_items(running_html, actual_items){

		$(".currently_running").html(running_html);
		//console.log(running_html);
		$.each( actual_items, function( key, queue_item ){

			// console.log(key);
			// console.log(queue_item);

			var queue_id = key;
			var action   = queue_item.action;
			var params   = $.trim(queue_item.action_params.replace(/[\t\n]+/g,' '))

			// console.log(queue_id);
			// console.log(action);
			// console.log(params);
		
			$.post("delete_data.php", {queue_id:queue_id}, function(response1){

				$(".queue_manage_div").remove();
				$(".tr_id_" + queue_id).remove();

				$.post("../post.php", {user:"sherpa", action:action, action_details:params}, function(response){

					// $(".queue_manage_div").remove();
					// $(".tr_id_" + queue_id).remove();
					//refresh_data();

				})

			})

		})

	}

	$(document.body).on("click", ".confirm_flush", function(){

		$(".flush_queue_info").html("<p>Please wait...flushing the queue</p><div class='currently_running'></div>");

		$count        = 0;
		$running      = "";
		$times        = 0;
		$actual_items = {};

		$.each( queue_data, function( key, queue_item ){

			// console.log(key);
			// console.log(queue_item);

			$count++;
			$running += "Running id: " + queue_item.id + " | action: " + queue_item.action + "<br>";
			$actual_items[key] = queue_item;

			if($count % 10 === 0){

				$count = 0;
				$times++;
				//$(".currently_running").html($running);

				//clear_items($running);

				window.setTimeout(clear_items, 5000 * $times, $running, $actual_items);
				$actual_items = {};
				//setTimeout(function(){ clear_items($running); }, 2000);

				$running = "";

				//sleep(2);

			}

		})

	})

	$(document.body).on("click", ".manage_queue_item", function(){

		var queue_id = $(this).attr("queue_id");
		$(".queue_manage_div").remove();

		var html_stuff = "";
		html_stuff += "<p style='font-size:16px'><strong>Action:</strong> " + queue_data[queue_id].action + "</p>";
		html_stuff += "<p style='font-size:16px'><strong>User:</strong> " + queue_data[queue_id].user + "</p>";
		html_stuff += "<p style='font-size:16px'><strong>Error:</strong> " + queue_data[queue_id].exec_result + "</p>";
		html_stuff += "<p style='font-size:16px'><strong>Action details:</strong> </p>";
		html_stuff += "<textarea class='form-control action_params' rows='5' >" + $.trim(queue_data[queue_id].action_params.replace(/[\t\n]+/g,' ')) + "</textarea><br><br>";
		html_stuff += "<button class='run_action btn btn-success' type='button' queue_id='" + queue_id + "' action_name='" + queue_data[queue_id].action + "'>Re-run</button> <button class='delete_action btn btn-danger' type='button' queue_id='" + queue_id + "' data-toggle='modal' data-target='#confirm-delete'>Delete</button>"

		$(".tr_id_" + queue_id).after("<tr class='queue_manage_div'><th colspan=8 style='border-top:none'><div>" + html_stuff + "</div></th></tr>")

	})

	$(document.body).on("click", ".confirm_delete", function(){

		var queue_id = $(".delete_action").attr("queue_id");

		$(".queue_manage_div").remove();
		$(".tr_id_" + queue_id).remove();

		$.post("delete_data.php", {queue_id:queue_id}, function(response){

			$('#confirm-delete').modal('hide');

		})

	})

	$(document.body).on("click", ".run_action", function(){

		var queue_id = $(this).attr("queue_id");
		var action   = $(this).attr("action_name");
		var params   = $(".action_params").val();

		console.log(queue_id);
		console.log(action);
		console.log(params);
	
		$.post("delete_data.php", {queue_id:queue_id}, function(response1){

			$.post("../post.php", {user:"sherpa", action:action, action_details:params}, function(response){

				$(".queue_manage_div").remove();
				$(".tr_id_" + queue_id).remove();
				refresh_data();

			})

		})

	})

	function refresh_data(){

		queue_data = {};

		$.post("fetch_data.php", {}, function(response){

			// console.log(response);
			$queue_table = "<table class='table table-hover queue_table'><thead><tr><th>ID</th><th>MQID</th><th>User</th><th>Action</th><th>Entered</th><th>Last ran</th><th>Status</th><th></th></tr></thead><tbody>";
			$queue       = jQuery.parseJSON(response);
			//console.log($queue);

			$.each( $queue, function( key, queue_item ) {

				queue_data[queue_item.id] = queue_item;

				$queue_table += "<tr class='queue_tr tr_id_" + queue_item.id + "'>";
				$queue_table += "<th>" + queue_item.id + "</th>";
				$queue_table += "<th>" + queue_item.mainQueueId + "</th>";
				$queue_table += "<th>" + queue_item.user + "</th>";
				$queue_table += "<th>" + queue_item.action + "</th>";
				//$queue_table += "<th>" + JSON.stringify(jQuery.parseJSON(queue_item.action_params), null, 2) + "</th>";
				//$queue_table += "<th>";
				//$queue_table += queue_item.action_params;
				// $.each( jQuery.parseJSON(queue_item.action_params), function( key, param ){

				// 	var param_string = "";

				// 	if(typeof param === 'string'){

				// 		param_string = param.replace(",", ", ");

				// 	}else if(typeof param === 'object'){

				// 		console.log(key);
				// 		console.log(param);

				// 		param_string += "{";

				// 		if(param){

				// 			$.each( param, function( key2, param2 ){

				// 				param_string += key2 + " : " + param2 + ", "; 

				// 			})					

				// 		}

				// 		param_string += "}";

				// 	}

				// 	$queue_table += key + " : " + param_string + "<br>";

				// })
				//$queue_table += "</th>";
				$queue_table += "<th>" + queue_item.entered + "</th>";
				$queue_table += "<th>" + queue_item.last_ran + "</th>";
				$queue_table += "<th>" + queue_item.status + "</th>";
				$queue_table += "<th><button queue_id='" + queue_item.id + "' class='manage_queue_item btn btn-primary' type='button'>Manage</button></th>";		
				$queue_table += "</tr>";

			})

			$queue_table += "</tbody></table>";
			$(".queue_div").html($queue_table);

		})

	}

	refresh_data();

})