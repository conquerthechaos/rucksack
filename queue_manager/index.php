<?php
    $today = date('Y-m-d');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Queue Manager</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  	<div role="navigation" class="navbar navbar-inverse"></div>
    <div role="main" class="container theme-showcase">
    	<div class="jumbotron">

    		<div class='main-content'>
    		<img src="img/Entrepreneurs-Circle-logo.png" width="400px" style="display:block;width:400px;margin:-30px auto 30px auto"> 
	    		<h2 class="text-center" >Queue Manager</h2>
	    		<button class='show_queue btn btn-warning' type='button' data-toggle='modal' data-target='#confirm-flush'>Flush queue</button><br><br><br>
	    		<div class='flush_queue_info'></div><br><br><br>
	    		<div class="row row-centered">
	    			<div class="col-md-12 center-block merge-message">
	    			</div>
				 	<div class="col-md-12 col-centered">
					 	<div class="panel panel-default">
						 	<div class="panel-heading">
						    	<h3 class="panel-title">Manage the queue<br/><small style="font-style:italic;font-size:14px">(just for managing the queue) </small></h3>
						 	</div>
						 	<div class="panel-body">

								<div class='queue_div'>Table here</div>

							</div>
						</div>

				 	</div>

				</div>

			</div>

    	</div>
    </div>

	<div class="modal fade" id="no_id" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-sm">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	        <h4 class="modal-title" id="myModalLabel">Error</h4>
	      </div>
	      <div class="modal-body">
	      	Please insert a contact id.
	      </div>
		  <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>

	<div class="modal fade" id="merge_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	        <h4 class="modal-title" id="myModalLabel">Confirm</h4>
	      </div>
	      <div class="modal-body confirm_modal_content">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-primary merge_modal_button">Merge</button>
	      </div>
	    </div>
	  </div>
	</div>

	<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
	                ...
	            </div>
	            <div class="modal-body">
	                Are you sure you want to delete this queue item?
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	                <a class="btn btn-danger btn-ok confirm_delete">Delete</a>
	            </div>
	        </div>
	    </div>
	</div>

	<div class="modal fade" id="confirm-flush" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
	                ...
	            </div>
	            <div class="modal-body">
	                Are you sure you want to flush the queue?
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
	                <a class="btn btn-danger btn-ok confirm_flush" data-dismiss="modal">Flush</a>
	            </div>
	        </div>
	    </div>
	</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
  </body>
</html>