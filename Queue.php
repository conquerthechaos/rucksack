<?php
/*
 * Queue
 *
 * Class that takes care of writting action results to the db (logger)
 *
 * @param $conn          (db pdo conn) db connection 
 * @param $user          (Int) user id
 * @param $action        (String) action name 
 * @param $action_params (String) json encoded array containing action params
 * @param $entered       (Int) timestamp of time the action was put into the queue
 * @return (Queue)
 */

	class Queue{

		public $conn;
		public $queue_id;
		public $user;
		public $action;
		public $action_params;
		public $entered;
		public $last_ran;
		public $exec_result;
		public $failed;
		public $mainQueueId;

		public function __construct($conn, $user, $action, $action_params, $entered){

			$this->conn          = $conn;
			$this->action        = $action;
			$this->action_params = $action_params;
			$this->entered       = $entered;
			$this->user          = $user;

		}

		/*
		* function notify_fails
		*
		* Checks to see if any queue item failed and sets a notification if it does (stops at first failed item so it only sends 1 notification)
		*
		* @param $queue_array (Array) Array of queue objects
		* @return null
		*/

		public static function notify_fails($queue_array){

			$alerts_done = file_get_contents("pagerduty_alerts.txt");
			$alerts_done = explode("\n", $alerts_done);
			$temp        = array();

			foreach($alerts_done as $alert){

				$alert           = explode(":", $alert);
				$temp[$alert[0]] = $alert[1];

			}

			$alerts_done = $temp;

			foreach($queue_array as $queue){

				if($queue->failed == 1 && !isset($alerts_done[$queue->mainQueueId])){

					//echo "<br>will notify";

					$description = "Item(s) in queue for more than 1 minute";
					// $details     = array(
					// 	"firstEl" => 1,
					// 	"secEl"   => 2
					// );
					$details = array(
						"mainQueueId" => $queue->mainQueueId
					);

					$Pagerduty      = new Pagerduty($details,$description);
					$pagerduty_json = $Pagerduty->post();
					$pagerduty_json = json_decode($pagerduty_json);

					file_put_contents("pagerduty_alerts.txt", $queue->mainQueueId. ":" . $pagerduty_json->incident_key . "\n", FILE_APPEND);

				}

			}

			return 1;

		}

		/*
		* function get_all
		*
		* Returns all objects in the queue and sets the fail prop. to 1 if it ran more than 1 min ago and it's still in the queue
		*
		* @param $conn (db pdo conn) db connection 
		* @return (Array) - returns an array of Queue objects
		*/

		public static function get_all($conn){

			$stmt = $conn->prepare("SELECT * FROM queue");
			$stmt->execute();
			$rows = $stmt->fetchAll();

			$queue_array = array();

			foreach($rows as $row){

				$entered  = "";
				$q_id     = $row['id'];
				$last_ran = "";

				try{

					$entered  = DateTime::createFromFormat("Y-m-d H:i:s", $row['entered'])->getTimestamp();
					$last_ran = DateTime::createFromFormat("Y-m-d H:i:s", $row['last_ran'])->getTimestamp();

				}catch(Exception $e){



				}

				$failed = 0;

				if( time() - $last_ran > 60 && $entered != ""){

					$failed = 1;

				}

				$queue_obj              = new Queue($conn, $row['user'], $row['action'], $row['action_params'], $entered);
				$queue_obj->queue_id    = $q_id;
				$queue_obj->last_ran    = $last_ran;
				$queue_obj->failed      = $failed;
				$queue_obj->mainQueueId = $row['mainQueueId'];
				$queue_array[]          = $queue_obj;

			}

			return $queue_array;

		}

		/*
		* function write
		*
		* Writes the Queue object details to the queue table and saves the id into queue_id
		*
		* @param none
		* @return null
		*/

		public function write(){

			$this->last_ran = time();

			$stmt = $this->conn->prepare("
				INSERT INTO queue(user,action,action_params,entered,last_ran) 
				VALUES(:user,:action,:action_params,:entered,:last_ran)
			");

			$stmt->execute(
				array(
					':user'          => $this->user["id"], 
					':action'        => $this->action,
					':action_params' => $this->action_params, 
					':entered'       => date("Y-m-d H:i:s", $this->entered),
					':last_ran'      => date("Y-m-d H:i:s"),				
				)
			);

			$lastId         = $this->conn->lastInsertId();
			$this->queue_id = $lastId;

			if($this->mainQueueId == 0){

				$this->mainQueueId = $this->queue_id;

			}

			$stmt = $this->conn->prepare("UPDATE queue SET mainQueueId = :mainQueueId WHERE id = " . $this->queue_id);
			$stmt->execute(array(":mainQueueId" => $this->mainQueueId));	

		}

		/*
		* function add_result
		*
		* If action was successfull deletes it from the queue table and adds the details into the success_action table else leaves it in the queue, updates status
		* to failed and adds the result into the failed_action table
		*
		* @param $exec_result (Array) Array containing action execution result
		* @param $success     (Bool) true if action ran successfull else false
		* @return null
		*/

		public function add_result($exec_result, $success){

			$this->exec_result = json_encode($exec_result);

			if($success){

				$stmt = $this->conn->prepare("
					INSERT INTO success_action(user,action,action_params,exec_result,entered,last_ran,mainQueueId) 
					VALUES(:user,:action,:action_params,:exec_result,:entered,:last_ran,:mainQueueId)
				");

				$stmt->execute(
					array(
						':user'          => $this->user["id"], 
						':action'        => $this->action,
						':action_params' => $this->action_params, 
						':exec_result'   => $this->exec_result,
						':entered'       => date("Y-m-d H:i:s", $this->entered),
						':last_ran'      => date("Y-m-d H:i:s", $this->last_ran),
						':exec_result'   => $this->exec_result,
						':mainQueueId'   => $this->mainQueueId,				
					)
				);

				$stmt = $this->conn->prepare("DELETE from queue WHERE id = :id");
				$stmt->execute(array(":id" => $this->queue_id));

			}else{

				$stmt = $this->conn->prepare("UPDATE queue SET exec_result = :exec_result, status = :status WHERE id = " . $this->queue_id);
				$stmt->execute(array(":exec_result" => $this->exec_result, ":status" => "failed"));		

				$stmt = $this->conn->prepare("
					INSERT INTO failed_action(queue_id,user,action,action_params,exec_result,entered,last_ran,mainQueueId) 
					VALUES(:queue_id,:user,:action,:action_params,:exec_result,:entered,:last_ran,:mainQueueId)
				");

				$stmt->execute(
					array(
						':queue_id'      => $this->queue_id,
						':user'          => $this->user["id"], 
						':action'        => $this->action,
						':action_params' => $this->action_params, 
						':exec_result'   => $this->exec_result,
						':entered'       => date("Y-m-d H:i:s", $this->entered),
						':last_ran'      => date("Y-m-d H:i:s", $this->last_ran),	
						':mainQueueId'   => $this->mainQueueId,				
					)
				);		

			}

		}

		public function set_main_queue($queue_id){

			$this->mainQueueId = $queue_id;

		}

	}

?>