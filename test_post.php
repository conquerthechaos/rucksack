<?php
	
	include_once('config/config.php');

	$queue_list = Queue::get_all($conn);


$_POST['user']           = "infusionsoft";
$_POST['action']         = "query_is";
$_POST['action_details'] = '
	{"table":"User", 
	 "limit":"1",
	 "page":0,
	 "fields":["FirstName", "Id", "LastName", "Email"],
	 "query":{"Id":"%"}
	}';

	$Rucksack = null;
	$headers  = apache_request_headers();

	$stmt = $conn->prepare("
		INSERT INTO calls(header,request,at,server) 
		VALUES(:header,:request,:at,:server)
	");

	$stmt->execute(
		array(
			':header'  => json_encode($headers), 
			':request' => json_encode($_REQUEST),
			':at'      => date("Y-m-d H:i:s"),	
			':server'  => json_encode($_SERVER)
		)
	);

	if(!isset($_POST['user']) || !isset($_POST['action']) || !isset($_POST['action_details'])){

	 	header("HTTP/1.1 400 Bad Request");
	 	echo "400 - Invalid param";
	    exit;	

	}

	$Rucksack    = new Rucksack($conn, $_POST['user'], $_POST['action'], $_POST['action_details'], $infusionsoft);

	$exec_result = $Rucksack->auth();

	if(isset($exec_result['status'])){

	 	header("HTTP/1.1 " . $exec_result['status']);
	 	echo $exec_result['message'];
	    exit;	

	}else{

		$exec_result = $Rucksack->execute();

		if(isset($exec_result['status'])){

		 	header("HTTP/1.1 " . $exec_result['status']);
		 	echo $exec_result['message'];
		    exit;		

		}else{

			header("Content-Type: text/plain");
			print_r($exec_result);
			exit;

		}

	}
?>
