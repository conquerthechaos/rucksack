<?php

session_start();

/**
 * Multi-Environment Config
 * 
 * Loads config file based on current environment, environment can be set
 * in either the environment variable 'ENV' or can be set based on the 
 * server hostname.
 * 
 * Common environment names are as follows, though you can use what you wish:
 * 
 *   production
 *   staging
 *   development
 * 
 * For each environment a config file must exist named config.{environment}.php
 * with any settings specific to that environment. For example a development 
 * environment would use the config file: wp-config.development.php
 * 
 * Default settings that are common to all environments can exist in wp-config.default.php
 * 
 * @package    Studio 24 WordPress Multi-Environment Config
 * @version    1.0
 * @author     Studio 24 Ltd  <info@studio24.net>
 */

// Try environment variable 'WHICH_ENV'
if (getenv('WHICH_ENV') !== false) {
    // Filter non-alphabetical characters for security
    define('WHICH_ENV', preg_replace('/[^a-z]/', '', getenv('WHICH_ENV')));
} 

// Are we in SSL mode?
if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') {
    $protocol = 'https://';
} else {
    $protocol = 'http://';
}

// Define site host
$cli = FALSE;
if (isset($_SERVER['X_FORWARDED_HOST']) && !empty($_SERVER['X_FORWARDED_HOST'])) {
    $hostname = $_SERVER['X_FORWARDED_HOST'];
} else if(isset($_SERVER['HTTP_HOST'])) {
    $hostname = $_SERVER['HTTP_HOST'];
} else {
    $hostname = php_uname('n');
    $cli = TRUE;
}

// Try server hostname
if (!defined('WHICH_ENV')) {
    // Set environment based on hostname
    include 'config.env.php';
}

/** Absolute path to the document root directory. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__DIR__) . '/');

// Load default config
include 'config.default.php';

// Load config file for current environment
include 'config.' . WHICH_ENV . '.php';

// Load application dependencies
require_once(dirname(dirname(dirname(dirname(__FILE__)))).'/vendor/autoload.php');
//require_once(dirname(dirname(__FILE__)).'/config/initialise_rollbar.php');
include_once(dirname(dirname(__FILE__))."/Queue.php");
include_once(dirname(dirname(__FILE__))."/actions/default_action.php");
include_once(dirname(dirname(__FILE__))."/actions/default_is_action.php");
include_once(dirname(dirname(__FILE__))."/Rucksack.php");


//db init
// global $conn;
// //$conn = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME,  DB_USER ,  DB_PASSWORD);
// $conn = new PDO("sqlite:" . ABSPATH . "rucksack.db");
// $conn->exec("pragma synchronous = off;");
// $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

global $conn;
//$conn = new PDO('mysql:host=' . DB_HOST . ';dbname=' . DB_NAME,  DB_USER ,  DB_PASSWORD);
try {
    $conn = new PDO("sqlite:" . ABSPATH . "rucksack.db");
} catch (Exception $e) {
    
}

// define('APPLICATION_KEY', 'hntj6gzgtgya9u5hjfu42292');
// define('APPLICATION_SECRET', 'kGjw9XpvwE');

//is init
global $infusionsoft;

try{
    $infusionsoft = new \Infusionsoft\Infusionsoft(array());

    $infusionsoft->setHttpClient(new \Infusionsoft\Http\CurlClient());

    /*=====================Infusionsoft Token Check===========================*/
    // if (isset($_SESSION['IS_Token'])) {
    //     $infusionsoft->setToken(unserialize($_SESSION['IS_Token']));
    // }else{
    //     //recyle token
    //     $StoredToken = file_get_contents(dirname(dirname(__FILE__)).'/temp/token.dat');
    //     if(trim($StoredToken) != ''){
    //         $infusionsoft->setToken(unserialize(trim($StoredToken)));
    //         //refresh token to prevent from expiration
    //         $infusionsoft->refreshAccessToken();
    //     }
    // }

    $now          = time();
    $file_changed = filemtime(dirname(dirname(__FILE__)).'/temp/token.dat');

    if($now - $file_changed > 28798){

        //sleep(3);

    }

    $StoredToken = file_get_contents(dirname(dirname(__FILE__)).'/temp/token.dat');
    $infusionsoft->setToken(unserialize(trim($StoredToken)));

    define('IS_Token', true);
    $_SESSION['IS_Token'] = serialize($infusionsoft->getToken());

}catch(Exception $e){


    $headers  = apache_request_headers();

    $stmt = $conn->prepare("
        INSERT INTO calls(header,request,at,server) 
        VALUES(:header,:request,:at,:server)
    ");

    $stmt->execute(
        array(
            ':header'  => json_encode($headers), 
            ':request' => json_encode($_REQUEST),
            ':at'      => date("Y-m-d H:i:s"),  
            ':server'  => json_encode($_SERVER)         
        )
    );

    //pagerduty notify if IS ureachable
    $description = "Infusionsoft is unreachable";
    $details     = array();

    $Pagerduty = new Pagerduty($details,$description);
    $Pagerduty->post();

    exit();

}
//exit();

unset($hostname, $protocol, $actual_link, $current_env);

/** End of Multi-Environment Config **/


/* That's all, stop editing! Happy blogging. */

if ( defined('DEBUG') && DEBUG == true ) {
    ini_set('display_errors',1);
    ini_set('display_startup_errors',1);
    error_reporting(-1);
}