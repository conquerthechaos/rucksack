<?php
/**
 * Default config settings
 *
 * Enter any config settings that are default to all environments
 * in this file. These can then be overridden in the environment config files.
 * 
 * Please note if you add constants in this file (i.e. define statements) 
 * these cannot be overridden in environment config files.
 * 
 * @package    Studio 24 WordPress Multi-Environment Config
 * @version    1.0
 * @author     Studio 24 Ltd  <info@studio24.net>
 */

// ** As per http://geekpad.ca/blog/post/maintainble-portable-wordpress-using-composer-wp-cli ** //

//require_once('vendor/autoload.php');

//require_once('initialise_rollbar.php');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');
