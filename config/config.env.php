<?php
/**
 * Setup environments
 * 
 * Set environment based on the current server hostname, this is stored
 * in the $hostname variable
 * 
 * You can define the current environment via: 
 *     define('WHICH_ENV', 'production');
 * 
 * @package    Studio 24 WordPress Multi-Environment Config
 * @version    1.0
 * @author     Studio 24 Ltd  <info@studio24.net>
 */


// Set environment based on hostname
switch ($hostname) {
    case strpos($hostname,"local") !== false:
        define('WHICH_ENV', 'development');
        break;
    case strpos($hostname,"staging") !== false:
    	define('WHICH_ENV', 'staging');
        break;
    default:
        define('WHICH_ENV', 'production');
        break;
}
