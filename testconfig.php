<?php
	include 'config/config.php';
	if (!$cli) {
		echo $rollbar_js."<br>";
		echo "CLI: False<br>";
	} else {
		echo "CLI: True\n";
	}
	echo "Environment: ".WHICH_ENV.(($cli == True) ? "\n" : "<br>");
	echo "Database Host: ".DB_HOST.(($cli == True) ? "\n" : "<br>");
	echo "Database Name: ".DB_NAME.(($cli == True) ? "\n" : "<br>");
	echo "Database User: ".DB_USER.(($cli == True) ? "\n" : "<br>");
	echo "ABSPATH: " . ABSPATH . (($cli == True) ? "\n" : "<br>"); 	
?>