<?php

/*
 * update_is
 *
 * Class that updates any table records inside infusionsoft (extends default_is_action)
 *
 * @param $conn          (db pdo conn) db connection - inherited from default_action
 * @param $action        (String) action name - inherited from default_action
 * @param $user          (User) contains the user object - inherited from default_action
 * @param $action_params (String) json encoded array containing action params - inherited from default_action
 * @param $isdk          (Infusionsoft api sdk) - inherited from default_is_action
 * @return (update_is)
 */

	class email_is extends default_is_action{

		public function __construct($conn, $action, $user, $action_params, $isdk){

			$this->action_perm_level = 3;

			parent::__construct($conn, $action, $user, $action_params, $isdk);

		}

		/*
		* function run
		*
		* runs the is action
		*
		* @param none
		* @return (Array) Returns either an error array or a success array
		*/

		public function run(){

			//add the action in the queue
			$this->queue->write();

			//user isn't allowed to run this action and it returns an error array
			if($this->allow != 1){

				$return_value = array(
					"message" => "401 - You don't have permission to run this action!",
					"status"  => "401 Unauthorized"
				);

				//send the run result to the queue object as a failed action
				$this->queue->add_result($return_value, false);	

			}else{
                $params = json_decode($this->action_params);
                foreach($params as $field => $value){

                    $$field = $value;

                }
                try{
                    //infusionsoft call is made after all the action params are checked and ok
                    $res = $this->isdk->emails()->sendEmail(array($contactList), (string)$fromAddress, (string)$toAddress, (string)$ccAddresses, (string)$bccAddresses, (string)$contentType, (string)base64_decode($subject), (string)base64_decode($htmlBody), (string)$textBody, (string)'');
                    //                                       java.lang.String,     java.lang.String,     java.lang.String,   java.lang.String,    java.lang.String,      java.lang.String,     java.lang.String,                java.lang.String,                 java.lang.String, java.lang.String
                    $return_value = array(
                        "message" => $res,
                    );
                    //send the run result to the queue object as a successfull action
                    $this->queue->add_result($return_value, true);

                }catch (Exception $e) {

                    $err = "";
                    //special error if the token has expired
                    if(get_class($e) == "Infusionsoft\TokenExpiredException"){

                        $err = "Infusionsoft token expired";

                    }else{

                        $err = $e->getMessage();

                    }

                    $return_value = array(
                        "message" => "error",
                        "error"   => $err
                    );

                    //send the run result to the queue object as a failed action
                    $this->queue->add_result($return_value, false);

                }

			}
            $return_value = json_encode($return_value);
			return $return_value;	

		}


	}

?>