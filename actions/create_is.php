<?php

/*
 * create_is
 *
 * Class that creates any table records inside infusionsoft (extends default_is_action)
 *
 * @param $conn          (db pdo conn) db connection - inherited from default_action
 * @param $action        (String) action name - inherited from default_action
 * @param $user          (User) contains the user object - inherited from default_action
 * @param $action_params (String) json encoded array containing action params - inherited from default_action
 * @param $isdk          (Infusionsoft api sdk) - inherited from default_is_action
 * @return (create_is)
 */

	class create_is extends default_is_action{

		public function __construct($conn, $action, $user, $action_params, $isdk){

			$this->action_perm_level = 3;

			parent::__construct($conn, $action, $user, $action_params, $isdk);

		}

		/*
		* function run
		*
		* runs the is action
		*
		* @param none
		* @return (Array) Returns either an error array or a success array
		*/

		public function run(){

			//add the action in the queue
			$this->queue->write();

			//user isn't allowed to run this action and it returns an error array
			if($this->allow != 1){

				$return_value = array(
					"message" => "401 - You don't have permission to run this action!",
					"status"  => "401 Unauthorized"
				);

				//send the run result to the queue object as a failed action
				$this->queue->add_result($return_value, false);	

			}else{

				$params = json_decode($this->action_params);
				//action call doesn't contain all required params (Table name, record id & fields) and it returns an error array
				if(!isset($params->table) || !isset($params->fields)){

					$return_value = array(
						"message" => "400 - Invalid params1",
						"status"  => "400 Bad Request1"
					);

					//send the run result to the queue object as a failed action
					$this->queue->add_result($return_value, false);	

				}else{

					$data = array();

					foreach($params->fields as $field => $value){

						$data[$field] = $value;

					}

					try{

						//infusionsoft call is made after all the action params are checked and ok
						$res = $this->isdk->data()->add($params->table, $data);

						$return_value = array(
							"message" => $res,
						);

						if(isset($params->optin)){

							$this->isdk->emails()->optIn($data['Email'],"Migrate opt-in status from m148");

						}

			            //send the run result to the queue object as a successfull action
						$this->queue->add_result($return_value, true);	

					}catch (Exception $e) {

						$err = "";
						//special error if the token has expired
						if(get_class($e) == "Infusionsoft\TokenExpiredException"){

							$err = "Infusionsoft token expired";

						}else{

							$err = $e->getMessage();

						}

					    $return_value = array(
							"message" => "error",
							"error"   => $err
						);
						
					    //send the run result to the queue object as a failed action
						$this->queue->add_result($return_value, false);	

					}			

				}

			}

			$return_value = json_encode($return_value);
			return $return_value;	

		}


	}

?>