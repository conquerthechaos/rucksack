<?php

/*
 * api_goal
 *
 * Class that applies any api goals to a specific contact (extends default_is_action)
 *
 * @param $conn          (db pdo conn) db connection - inherited from default_action
 * @param $action        (String) action name - inherited from default_action
 * @param $user          (User) contains the user object - inherited from default_action
 * @param $action_params (String) json encoded array containing action params - inherited from default_action
 * @param $isdk          (Infusionsoft api sdk) - inherited from default_is_action
 * @return (update_is)
 */

	class api_goal extends default_is_action{

		public function __construct($conn, $action, $user, $action_params, $isdk){

			$this->action_perm_level = 3;

			parent::__construct($conn, $action, $user, $action_params, $isdk);

		}

		/*
		* function run
		*
		* runs the is action
		*
		* @param none
		* @return (Array) Returns either an error array or a success array
		*/

		public function run(){

			//add the action in the queue
			$this->queue->write();

			//user isn't allowed to run this action and it returns an error array
			if($this->allow != 1){

				$return_value = array(
					"message" => "401 - You don't have permission to run this action!",
					"status"  => "401 Unauthorized"
				);


			//send the run result to the queue object as a failed action
			$this->queue->add_result($return_value, false);	

			}else{

				$params = json_decode($this->action_params);

				//action call doesn't contain all required params (goal id & contact id) and it returns an error array
				if(!isset($params->goal_id) || !isset($params->contact_id)){

					$return_value = array(
						"message" => "400 - Invalid params",
						"status"  => "400 Bad Request"
					);

					//send the run result to the queue object as a failed action
					$this->queue->add_result($return_value, false);	

				}else{

					//gets the goal details from the db
					$sqlSelect = 'SELECT * FROM `api_goal` WHERE id = :id';
					$sth       = $this->conn->prepare($sqlSelect);

					$sth->execute(array(':id' => $params->goal_id));
					$result = $sth->fetchAll();

					//goal isn't found and it returns an error array
					if(count($result) == 0){

						$return_value = array(
							"message" => "400 - Invalid params | Goal not found",
							"status"  => "400 Bad Request"
						);

						//send the run result to the queue object as a failed action
						$this->queue->add_result($return_value, false);	

					}else{

						try{

							$goal_suffix = "";

							if(isset($params->goal_suffix)){

								$goal_suffix = $params->goal_suffix;

							}

							if($result[0]['callname'] == "PaymentProcessed"){

								$result[0]['callname'] = "Payment";

							}

							if($result[0]['callname'] == "Cancellation"){

								$result[0]['callname'] = "Cancel";

							}

							$result[0]['callname'] .= $goal_suffix;

							$res           = $this->isdk->funnels()->achieveGoal($result[0]['integration_name'], $result[0]['callname'], $params->contact_id);
							$res['suffix'] = $goal_suffix;

							if($res[0]['success']){

								$return_value = array(
									"message" => $res,
								);	

					            //send the run result to the queue object as a successfull action
								$this->queue->add_result($return_value, true);	

							}else{

								$return_value = array(
									"message" => "error",
									"error"   => $res
								);	

					            //send the run result to the queue object as a failed action
								$this->queue->add_result($return_value, false);	

							}

						}catch (Exception $e) {

							$err = "";
							//special error if the token has expired
							if(get_class($e) == "Infusionsoft\TokenExpiredException"){

								$err = "Infusionsoft token expired";

							}else{

								$err = $e->getMessage();

							}

						    $return_value = array(
								"message" => "error",
								"error"   => $err
							);

						    //send the run result to the queue object as a failed action
							$this->queue->add_result($return_value, false);	

						}

					}
							
				}

			}

			return $return_value;	

		}


	}

?>