<?php

	class apply_tag extends default_is_action{

		public function __construct($conn, $action, $user, $action_params, $isdk){

			$this->action_perm_level = 3;

			parent::__construct($conn, $action, $user, $action_params, $isdk);

		}

		/*
		* function run
		*
		* runs the is action
		*
		* @param none
		* @return (Array) Returns either an error array or a success array
		*/

		public function run(){

			//add the action in the queue
			$this->queue->write();

			//user isn't allowed to run this action and it returns an error array
			if($this->allow != 1){

				$return_value = array(
					"message" => "401 - You don't have permission to run this action!",
					"status"  => "401 Unauthorized"
				);

				//send the run result to the queue object as a failed action
				$this->queue->add_result($return_value, false);	

			}else{

				$params = json_decode($this->action_params);

				//action call doesn't contain all required params (Table name & record id) and it returns an error array
				if(!isset($params->tag) || !isset($params->id)){

					$return_value = array(
						"message" => "400 - Invalid params",
						"status"  => "400 Bad Request"
					);

					//send the run result to the queue object as a failed action
					$this->queue->add_result($return_value, false);		

				}else{

					try{

						//infusionsoft call is made after all the action params are checked and ok
						$res = $this->isdk->contacts()->addToGroup($params->id, $params->tag);

						$return_value = array(
							"message" => $res,
						);

			            //send the run result to the queue object as a successfull action
						$this->queue->add_result($return_value, true);	

					}catch (Exception $e) {

						$err = "";
						//special error if the token has expired
						if(get_class($e) == "Infusionsoft\TokenExpiredException"){

							$err = "Infusionsoft token expired";

						}else{

							$err = $e->getMessage();

						}

					    $return_value = array(
							"message" => "error",
							"error"   => $err
						);
						
					    //send the run result to the queue object as a failed action
						$this->queue->add_result($return_value, false);	

					}			

				}

			}

			return $return_value;	

		}


	}

?>