<?php


	class get_tw_url extends default_is_action{

		public function __construct($conn, $action, $user, $action_params, $isdk){

			$this->action_perm_level = 3;

			parent::__construct($conn, $action, $user, $action_params, $isdk);

		}

		/*
		* function run
		*
		* runs the is action
		*
		* @param none
		* @return (Array) Returns either an error array or a success array
		*/

		public function run(){

			//add the action in the queue
			$this->queue->write();

			//user isn't allowed to run this action and it returns an error array
			if($this->allow != 1){

				$return_value = array(
					"message" => "401 - You don't have permission to run this action!",
					"status"  => "401 Unauthorized"
				);

				//send the run result to the queue object as a failed action
				$this->queue->add_result($return_value, false);	

			}else{

				$params = json_decode($this->action_params);
				//action call doesn't contain all required params (Table name, record id & fields) and it returns an error array
				if( !isset($params->contactId) || !isset($params->TEAMWORK_TOKEN) || !isset($params->TEAMWORK_URL)){

					$return_value = array(
						"message" => "400 - Invalid params1",
						"status"  => "400 Bad Request1"
					);

					//send the run result to the queue object as a failed action
					$this->queue->add_result($return_value, false);	

				}else{


					try{
						//_NetsuiteId
						//$res = $this->isdk->data()->query((string)$params->table, (int)$params->limit, (int)$params->page, $query_by, $params->fields, "", false);

						$query_by = array(
							"Id" => $params->contactId
						);

						$fields       = array("Id", "FirstName", "LastName", "Company", "Email", "_NetsuiteID", "_Clientof", "_QSPackage", '_KickStartAssignment');
						$contact_data = $this->isdk->data()->query("Contact", 10, 0, $query_by, $fields, "", false);

						$firstname = "";
						$lastname  = "";
						$company   = "";
						$email     = "";
						$proj_name = "Test project name";
						$username  = "";

						// echo '<pre>';
						// 	print_r($contact_data);
						// echo '</pre>';

						define('TEAMWORK_TOKEN', $params->TEAMWORK_TOKEN);
						define('TEAMWORK_URL', $params->TEAMWORK_URL);

						// define('TEAMWORK_TOKEN', 'crust251man');
						// define('TEAMWORK_URL', 'https://exela.teamwork.com/');

						$project_found = false;

						if(isset($contact_data[0]['_NetsuiteID'])){

							$teamwork_projects = $this->get_teamwork_data("/projects.json");
							$teamwork_projects = json_decode($teamwork_projects['body']);
							$teamwork_projects = $teamwork_projects->projects;

							$url = "";

							foreach($teamwork_projects as $project){

								if(strpos(strtolower($project->name), strtolower($contact_data[0]['_NetsuiteID'])) !== false){

									$project_found = true;
									$url           = TEAMWORK_URL . "projects/" . $project->id . "/";

								}

							}

							if($project_found){

								$return_value = array(
									"message" => $url
								);

								//send the run result to the queue object as a successfull action
								$this->queue->add_result($return_value, true);	

							}


						}


						if(!$project_found){

							if(!isset($contact_data[0]['_KickStartAssignment'])){

							    $return_value = array(
									"message" => "error",
									"error"   => "no coach assigned"
								);
								
							    //send the run result to the queue object as a failed action
								$this->queue->add_result($return_value, false);	

							}else{

								$same_template_id = 501872;

								// $template_assignments = array(
								// 	"jamie"   => array(
								// 		"template_id" => $same_template_id,
								// 		"user_id"     => 93701
								// 	),
								// 	"andy"    => array(
								// 		"template_id" => $same_template_id,
								// 		"user_id"     => 112798
								// 	),
								// 	"insa"    => array( 
								// 		"template_id" => $same_template_id,
								// 		"user_id"     => 101305
								// 	),
								// 	"rachael" => array(
								// 		"template_id" => $same_template_id,
								// 		"user_id"     => 112071
								// 	),
								// 	"james"   => array(
								// 		"template_id" => $same_template_id,
								// 		"user_id"     => 108439
								// 	),
								// 	"andrea"   => array(
								// 		"template_id" => $same_template_id,
								// 		"user_id"     => 116066
								// 	),
								// 	"scott"   => array(
								// 		"template_id" => $same_template_id,
								// 		"user_id"     => 116065
								// 	),

								// );


								$teamwork_staff = $this->get_teamwork_data("/companies/35163/people.json");
								$teamwork_staff = json_decode($teamwork_staff['body']);

								$template_assignments = array();

								foreach($teamwork_staff->people as $individual){

									$template_assignments[$individual->{'first-name'} . " " . $individual->{'last-name'}] = array(
										"template_id" => 501872,
										"user_id"     => $individual->id
									);

								}

								$proj_cat_array = array(
									"iKOP"       => 7669,
									"Launch"     => 7670,
									"LaunchPLUS" => 7671
								);

								$cat_id = 0;

								if(isset($contact_data['0']['_Clientof'])){

									if(isset($proj_cat_array[$contact_data['0']['_Clientof']])){

										$cat_id = $proj_cat_array[$contact_data['0']['_Clientof']];

									}else if(isset($proj_cat_array[$contact_data['0']['_QSPackage']])){

										$cat_id = $proj_cat_array[$contact_data['0']['_QSPackage']];

									}

								}

								$coach_name  = $contact_data[0]['_KickStartAssignment'];
								$template_id = $template_assignments[$coach_name]['template_id'];
								$coach_id    = $template_assignments[$coach_name]['user_id'];

								if(isset($contact_data[0]['FirstName'])){

									$firstname = $contact_data[0]['FirstName'];

								}

								if(isset($contact_data[0]['LastName'])){

									$lastname = $contact_data[0]['LastName'];

								}

								if(isset($contact_data[0]['Email'])){

									$email = $contact_data[0]['Email'];

								}

								if(isset($contact_data[0]['Company'])){

									$company = $contact_data[0]['Company'];

								}else{

									if(isset($contact_data[0]['_NetsuiteID'])){

										$company = explode(" ", $contact_data[0]['_NetsuiteID']);
										unset($company[0]);
										$company = implode(" ", $company);

									}else{

										$company = $firstname . " " . $lastname;

									}

								}

								if(isset($contact_data[0]['_NetsuiteID'])){

									$proj_name = $contact_data[0]['_NetsuiteID'];

								}else{

									$proj_name = $params->contactId . " " . $firstname . " " . $lastname;

								}

								$username = $firstname . " " . $lastname;

								$teamwork_company = $this->create_teamwork_company($company);
								$tw_company_id    = $teamwork_company['id'];

								$teamwork_user    = $this->create_teamwork_user($firstname, $lastname, $tw_company_id, $email, $username);
								$teamwork_user_id = $teamwork_user['id'];

								$teamwork_project = $this->create_teamwork_project($proj_name, $tw_company_id, $cat_id);
								$tw_project_id    = $teamwork_project['id'];

								$assign_res1   = $this->assign_teamwork_project($teamwork_user_id, $tw_project_id);
								$assign_res2   = $this->assign_teamwork_project($coach_id, $tw_project_id);

								$assign_res = array($assign_res1, $assign_res2);

								$tasklist_res = $this->create_teamwork_tasklist($template_id, $tw_project_id, $teamwork_user_id, $coach_id);

								//new
								//$tasklist_res = $this->create_teamwork_tasklist($template_id, $tw_project_id, false, $coach_id);


								$permissions_res = $this->give_admin_permission($coach_id, $tw_project_id);

								$return_value = array(
									"message" => TEAMWORK_URL . "projects/" . $tw_project_id . "/"
								);

								//send the run result to the queue object as a successfull action
								$this->queue->add_result($return_value, true);	

							}

						}

					}catch (Exception $e) {

						$err = "";
						//special error if the token has expired
						if(get_class($e) == "Infusionsoft\TokenExpiredException"){

							$err = "Infusionsoft token expired";

						}else{

							$err = $e->getMessage();

						}

					    $return_value = array(
							"message" => "error",
							"error"   => $err
						);
						
					    //send the run result to the queue object as a failed action
						$this->queue->add_result($return_value, false);	

					}			

				}

			}

			$return_value = json_encode($return_value);
			return $return_value;	

		}


	}

?>