<?php

/*
 * default_action
 *
 * Main action class
 *
 * @param $conn          (db pdo conn) db connection
 * @param $action        (String) action name
 * @param $user          (User) contains the user object
 * @param $action_params (String) json encoded array containing action params
 * @return (default_action)
 */

	class default_action{

		public $action;
		public $user;
		public $action_params;
		public $action_perm_level;
		public $allow;
		public $conn;
		public $queue;
		public $zuora_contact_field        = "InfusionsoftContactId__c";
		public $infusionsoft_account_field = "_ZuoraAccountNumber";
		public $mainQueueId;
		public $products;
		public $sherpa_goal_id;

		public function __construct($conn, $action, $user, $action_params){

			$this->action        = $action;
			$this->user          = $user;
			$this->action_params = $action_params;
			$this->allow         = 0;
			$this->conn          = $conn;
			$this->queue         = new Queue($conn, $user, $action, $action_params, time());

			$this->get_sherpa_goal();

		}

		/*
		* function check_perm_level
		*
		* Checks to see if the user is allowed to run the action
		*
		* @param none
		* @return (Array || Int) Returns an error message array if user doesn't have permission else returns 1
		*/

		public function check_perm_level(){

			if($this->user["permission_level"] < $this->action_perm_level){

				$return_value = array(
					"message" => "401 - You don't have permission to run this action",
					"status"  => "401 Unauthorized"
				);

				return $return_value;

			}else{

				$this->allow = 1;
				return "1";

			}

		}

		public function set_main_queue($queue_id){

			$this->mainQueueId = $queue_id;
			$this->queue->set_main_queue($this->mainQueueId);

		}

		public function generate_random_number($numbers, $min, $max){

			include_once(ABSPATH . "RandomOrgClient.php");
			$random       = new RandomOrgClient();
			$arrRandomInt = $random->generateIntegers($numbers, $min, $max);

			return $arrRandomInt;

		}

		public function do_curl_post($post_fields, $header, $url, $method){

			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

			$result = curl_exec($ch);

			return $result;

		}

		public function dbox_list_folder($folder_path, $recursive = false){

			$header = array(
				"Authorization: Bearer " . DROPBOX_TOKEN,
				"Content-Type: application/json"
			);

			$post_data = array(
				"path"      => $folder_path,
				"recursive" => $recursive
			);

			$post_data = json_encode($post_data);
			$data      = $this->do_curl_post($post_data, $header, "https://api.dropboxapi.com/2/files/list_folder", "POST");

			return $data;

		}

		public function dbox_save_file($filename, $temp_file){

			$header_data = array(
				"path"       => $filename,
				"mode"       => "add",
                "autorename" => true,
                "mute"       => false,
			);	

			$header = array(
				"Authorization: Bearer " . DROPBOX_TOKEN,
				"Dropbox-API-Arg: " . json_encode($header_data),
				"Content-Type: application/octet-stream",
			);

			$post_data = $temp_file;

			//print_r($post_data);

			//$post_data = json_encode($post_data);
			$data      = $this->do_curl_post($post_data, $header, "https://content.dropboxapi.com/2/files/upload", "POST");

			return $data;

		}

		public function dbox_get_root_content(){

			return json_decode($this->dbox_list_folder(""));

		}

		public function dbox_create_folder($path){

			$header = array(
				"Authorization: Bearer " . DROPBOX_TOKEN,
				"Content-Type: application/json"
			);

			$post_data = array(
				"path"      => $path,
			);	

			$post_data = json_encode($post_data);
			$data      = $this->do_curl_post($post_data, $header, "https://api.dropboxapi.com/2/files/create_folder", "POST");

			return $data;

		}

		public function dbox_get_link($path){

			$header = array(
				"Authorization: Bearer " . DROPBOX_TOKEN,
				"Content-Type: application/json"
			);

			$post_data = array(
				"path"     => $path,
				"settings" => array(
					"requested_visibility" => "public"
				)
			);	

			$post_data = json_encode($post_data);
			$data      = $this->do_curl_post($post_data, $header, "https://api.dropboxapi.com/2/sharing/create_shared_link_with_settings", "POST");

			return $data;

		}

		public function get_groove_tickets_by_email($email){

			@$data = file_get_contents("https://api.groovehq.com/v1/tickets?per_page=50&page=1&customer=$email&access_token=" . GROOVE_TOKEN);

			return json_decode($data);

		}

		public function get_groove_agents(){

			$data = file_get_contents("https://api.groovehq.com/v1/agents?per_page=50&page=1&access_token=" . GROOVE_TOKEN);
			$data = json_decode($data);

			$parsed_agents = array();

			foreach($data->agents as $agent){

				$first_name = "";
				$last_name  = "";

				if(isset($agent->first_name)){

					$first_name = $agent->first_name;

				}

				if(isset($agent->last_name)){

					$last_name = $agent->last_name;

				}

				$parsed_agents[$agent->href] = $first_name . " " . $last_name;

			}

			return $parsed_agents;

		}

		public function get_sherpa_goal(){

			$stmt = $this->conn->prepare("SELECT * FROM api_goal WHERE callname='SherpaAlert'");
			$stmt->execute();
			$api_goal = $stmt->fetchAll();

			//$this->sherpa_goal_id = $api_goal[0]['id'];

		}

		public function get_card_type($card_number){

			include_once(ABSPATH . "CreditcardType.php");
			return CreditcardType::getType($card_number);

		}

		public function gc_make_curl_post($request_data){

			$url = API_URL . $request_data['requested_data'];
			$ch  = curl_init();
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $request_data['request_type']);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'GoCardless-Version: '.GC_VERSION,
					"Authorization: Bearer ".GC_TOKEN,
					"Content-Type: application/json")
			);

			if(isset($request_data['fields'])){
				$data_string = json_encode($request_data['fields']);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);

			}
			$output = curl_exec($ch);
			$error  = curl_error($ch);
//			echo "<pre>";
//				print_r(json_decode($output));
//			echo "</pre>";
			curl_close($ch);
			return $output;
		}

		public function gc_search_by_email($email, $after = '', $before = ''){

			$found_users = array();

			do {
				$customers = $this->gc_get_customers(null,$after,$before, 500);
				$customers = json_decode($customers);
				$after = $customers->meta->cursors->after;

				foreach($customers->customers as $customer){
					if( $customer->email == $email){
						$found_users[] = $customer;
					}
				}
			} while (sizeof($customers->customers) >= 500);
			//return false;

			return $found_users;

		}

		public function gc_get_customers($id = null, $after = '', $before = '' , $limit = 500){
			$query = array();
			$query_str = '';
			if($after != '') { $query[] = "after=".$after; }
			if($before != '') { $query[] = "before=".$before; }
			$query[] = "limit=".$limit;
			$query_str = "?".implode('&',$query);
			if($id != null){
				$query_str = "/".$id;
			}
			$data                   = array();
			$data['requested_data'] = "customers".$query_str;
			$data['request_type']   = "GET";
			$raw_accounts_data      = $this->gc_make_curl_post($data);
			return $raw_accounts_data;

		}

		public function gc_get_mandates($id = null, $after = '', $before = '' , $limit = 500){
			$query = array();
			$query_str = '';
			if($after != '') { $query[] = "after=".$after; }
			if($before != '') { $query[] = "before=".$before; }
			$query[] = "limit=".$limit;
			$query_str = "?".implode('&',$query);
			if($id != null){
				$query_str = "/".$id;
			}
			$data                   = array();
			$data['requested_data'] = "mandates".$query_str;
			$data['request_type']   = "GET";
			$raw_accounts_data      = $this->gc_make_curl_post($data);
			return $raw_accounts_data;

		}

		function gc_mandate_search_by_contactId($contactId, $after = '', $before = ''){
			do {
				$mandates = $this->gc_get_mandates(null,$after,$before, 500);
				$mandates = json_decode($mandates);
				if(!isset($mandates->mandates)) return false;
				$after = $mandates->meta->cursors->after;
				//$before = $mandates->meta->cursors->before;
				foreach($mandates->mandates as $mandate){
					if(isset($mandate->metadata->Debtor_InfusionSoft_Id)){
						if($mandate->metadata->Debtor_InfusionSoft_Id == $contactId && $mandate->status != "cancelled") return $mandate;

					}
				}
			} while (sizeof($mandates->mandates) >= 500);
			return false;


		}

		public function do_curl_post_teamwork($post_fields, $url, $method, $header = false){

			if(!$header){

				$header = array(
					"Authorization: Basic " . base64_encode(TEAMWORK_TOKEN . ":xx"),
					"Content-Type: application/json",
				);			

			}

			$url = TEAMWORK_URL . $url;

			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post_fields);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
			curl_setopt($ch, CURLOPT_HEADER, 1);

			$result = curl_exec($ch);

			$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
			$header      = substr($result, 0, $header_size);
			$body        = substr($result, $header_size);

			$header = explode("\n", $header);
			$id     = 0;
			foreach($header as $piece){

				if(strpos($piece, "id") === 0){

					$id = explode(":", $piece);
					$id = trim($id[1]);

				}

			}

			$result = array(
				"id"    => $id,
				"body"  => $body
			);

			return $result;

		}

		public function get_teamwork_data($url){

			return $this->do_curl_post_teamwork("", $url, "GET");

		}

		public function create_teamwork_company($company_name){

			$post_fields = array(
				"company" => array(
					"name"      => $company_name,
				)
			);

			$post_fields = json_encode($post_fields);

			return $this->do_curl_post_teamwork($post_fields, "companies.json", "POST");

		}

		public function create_teamwork_user($firstname, $lastname, $company_id, $email, $username){

			$post_fields = array(
				"person" => array(
					"first-name"    => $firstname,
					"last-name"     => $lastname,
					"company-id"    => $company_id,
					"email-address" => $email,
					"user-name"     => $username
				)
			);

			$post_fields = json_encode($post_fields);

			return $this->do_curl_post_teamwork($post_fields, "people.json", "POST");

		}

		public function create_teamwork_project($project_name, $company_id, $cat_id = 0){

			$post_fields = array(
				"project" => array(
					"name"        => $project_name,
					"companyId"   => $company_id,
					"category-id" => $cat_id
				)
			);

			$post_fields = json_encode($post_fields);

			return $this->do_curl_post_teamwork($post_fields, "projects.json", "POST");

		}

		public function create_teamwork_tasklist($list_id, $project_id, $owner = false, $coach = false){

			$post_fields = array(
				"todo-list" => array(
					"todo-list-template-id"                => $list_id,
					"todo-list-template-keep-off-weekends" => true
				)
			);

			if($owner){

				$post_fields["todo-list"]["todo-list-template-assignments"] = array(

					"Business Owner" => $owner,
					"Coach" => $coach

				);

			}

			// if($coach){

			// 	$post_fields["todo-list"]["todo-list-template-assignments"] = array(

			// 		"Coach" => $coach

			// 	);			

			// }

			$post_fields = json_encode($post_fields);

			return $this->do_curl_post_teamwork($post_fields, "projects/" . $project_id . "/tasklists.json", "POST");

		}

		public function assign_teamwork_project($user_id, $project_id){

			$post_fields = array(
				"add" => array(
					"userIdList" => $user_id
				)
			);

			echo "userid: $user_id";

			$post_fields = json_encode($post_fields);

			return $this->do_curl_post_teamwork($post_fields, "projects/" . $project_id . "/people.json", "POST");

		}

		public function give_admin_permission($user_id, $project_id){

			$post_fields = array(
				"permissions" => array(
			    	"view-messages-and-files"                 => "1",
			    	"view-tasks-and-milestones"               => "1",
			    	"view-time"                               => "1",
			    	"view-notebooks"                          => "1",
			    	"view-risk-register"                      => "1",
			        "view-invoices"                           => "1",
			    	"view-links"                              => "1",
			    	"add-tasks"                               => "1",
			    	"add-milestones"                          => "1",
			    	"add-taskLists"                           => "1",
			    	"add-messages"                            => "1",
			    	"add-files"                               => "1",
			    	"add-time"                                => "1",
			    	"add-notebooks"                           => "1",
			    	"add-links"                               => "1",
			    	"set-privacy"                             => "1",
			    	"is-observing"                            => "1",
			    	"can-be-assigned-to-tasks-and-milestones" => "1",
			    	"project-administrator"                   => "1",
			    	"add-people-to-project"                   => "1"
			  	)
			);

			$post_fields = json_encode($post_fields);

			return $this->do_curl_post_teamwork($post_fields, "projects/" . $project_id . "/people/" . $user_id . ".json", "PUT");		

		}

	}

?>