<?php

/*
 * 
 *
 * Class that creates any table records inside infusionsoft (extends default_is_action)
 *
 * @param $conn          (db pdo conn) db connection - inherited from default_action
 * @param $action        (String) action name - inherited from default_action
 * @param $user          (User) contains the user object - inherited from default_action
 * @param $action_params (String) json encoded array containing action params - inherited from default_action
 * @param $isdk          (Infusionsoft api sdk) - inherited from default_is_action
 * @return 
 */

	class dropbox_save_file extends default_is_action{

		public function __construct($conn, $action, $user, $action_params, $isdk){

			$this->action_perm_level = 3;

			parent::__construct($conn, $action, $user, $action_params, $isdk);

		}

		/*
		* function run
		*
		* runs the is action
		*
		* @param none
		* @return (Array) Returns either an error array or a success array
		*/

		public function run(){

			//add the action in the queue
			$this->queue->write();

			//user isn't allowed to run this action and it returns an error array
			if($this->allow != 1){

				$return_value = array(
					"message" => "401 - You don't have permission to run this action!",
					"status"  => "401 Unauthorized"
				);

				//send the run result to the queue object as a failed action
				$this->queue->add_result($return_value, false);	

			}else{

				$params       = json_decode($this->action_params);
				$return_value = array();

				if(!isset($params->contact_id) ){

					$return_value = array(
						"message" => "400 - Invalid params1",
						"status"  => "400 Bad Request1"
					);

					//send the run result to the queue object as a failed action
					$this->queue->add_result($return_value, false);	

				}else{

					try{

						$root_content = $this->dbox_get_root_content();
						$found_folder = "";

						foreach($root_content->entries as $entry){

							if( $entry->{".tag"} == "folder" ){

								$explode_name = explode(" ", $entry->name);
								$name_array   = array();

								foreach($explode_name as $piece){

									$name_array[$piece] = $piece;

								}

								if(array_key_exists($params->contact_id, $name_array)){

									$found_folder = $entry->path_lower;
									break;									

								}

							}

						}

						if($found_folder == ""){

							$contact_info = $this->isdk->data()->query("Contact", 10, 0, array("Id" => $params->contact_id), array("FirstName", "LastName"), "", false);

							$found_folder = "/" . $contact_info[0]['LastName'] . " " . $contact_info[0]['FirstName'] . " " . $params->contact_id;
							$this->dbox_create_folder($found_folder);

							//$return_value['files'] = false;

						}

						// $file_created = time() . $params->filename;
						// file_put_contents($file_created, base64_decode($params->file_contents));

						//$return_value = $this->dbox_save_file($found_folder . "/" . $params->filename, $file_created);

						$return_value = $this->dbox_save_file($found_folder . "/" . $params->filename, base64_decode($params->file_contents));

			            //send the run result to the queue object as a successfull action
						$this->queue->add_result($return_value, true);	

					}catch (Exception $e) {

						$err = "";
						//special error if the token has expired
						if(get_class($e) == "Infusionsoft\TokenExpiredException"){

							$err = "Infusionsoft token expired";

						}else{

							$err = $e->getMessage();

						}

					    $return_value = array(
							"message" => "error",
							"error"   => $err
						);
						
					    //send the run result to the queue object as a failed action
						$this->queue->add_result($return_value, false);	

					}			

				}

			}

			$return_value = json_encode($return_value);
			return $return_value;	

		}


	}

?>