<?php

/*
 * update_is
 *
 * Class that updates any table records inside infusionsoft (extends default_is_action)
 *
 * @param $conn          (db pdo conn) db connection - inherited from default_action
 * @param $action        (String) action name - inherited from default_action
 * @param $user          (User) contains the user object - inherited from default_action
 * @param $action_params (String) json encoded array containing action params - inherited from default_action
 * @param $isdk          (Infusionsoft api sdk) - inherited from default_is_action
 * @return (update_is)
 */

	class update_is extends default_is_action{

		public function __construct($conn, $action, $user, $action_params, $isdk){

			$this->action_perm_level = 3;

			parent::__construct($conn, $action, $user, $action_params, $isdk);

		}

		/*
		* function run
		*
		* runs the is action
		*
		* @param none
		* @return (Array) Returns either an error array or a success array
		*/

		public function run(){

			//add the action in the queue
			$this->queue->write();

			//user isn't allowed to run this action and it returns an error array
			if($this->allow != 1){

				$return_value = array(
					"message" => "401 - You don't have permission to run this action!",
					"status"  => "401 Unauthorized"
				);

				//send the run result to the queue object as a failed action
				$this->queue->add_result($return_value, false);	

			}else{

				$params = json_decode($this->action_params);

				//action call doesn't contain all required params (Table name, record id & fields that needs updating) and it returns an error array
				if(!isset($params->table) || !isset($params->id) || !isset($params->fields)){

					$return_value = array(
						"message" => "400 - Invalid params",
						"status"  => "400 Bad Request"
					);

					//send the run result to the queue object as a failed action
					$this->queue->add_result($return_value, false);	

				}else{

					/*$queue_tries = 0;

					while($this->check_queue($params->table, $params->id) != false){

						sleep(1);
						$queue_tries++;

						if($queue_tries > 10){

							$return_value = array(
								"message" => "408 - Too many retries - queue might be stuck",
								"status"  => "408 Request Timeout"
							);

							return $return_value;	

							//do a pagerduty trigger as soon as a queue item is in the queue for longer than 1 minue
							
							//	pager duty service integration key = d0b1c87b77474c8c971a1d810a913bd1 
							//	pagerduty api key = BG9hkNJxo3M4snpSgRz1
							
							//as soon as the action comes troug store it in the queue (all details + time it came)
							//move successful queue items in the success log and also add last execution time as a field
							//put unsuccesfull action calls in the error log with the new fields last execution time and error code
							//add a queue/error log manager

						}

					}

					$this->set_queue($params->table, $params->id);*/

					$data = array();

					foreach($params->fields as $field => $value){

						$data[$field] = $value;

					}

					try{
						//infusionsoft call is made after all the action params are checked and ok
						$res = $this->isdk->data()->update($params->table, $params->id, $data);

						$return_value = array(
							"message" => $res,
						);
                        if(isset($params->optin)){

                            $this->isdk->emails()->optIn($data['Email'],"Macanta opt-in");

                        }
			            //send the run result to the queue object as a successfull action
						$this->queue->add_result($return_value, true);	

					}catch (Exception $e) {

						$err = "";
						//special error if the token has expired
						if(get_class($e) == "Infusionsoft\TokenExpiredException"){

							$err = "Infusionsoft token expired";

						}else{

							$err = $e->getMessage();

						}

					    $return_value = array(
							"message" => "error",
							"error"   => $err
						);

					    //send the run result to the queue object as a failed action
						$this->queue->add_result($return_value, false);	

					}			

				}

			}

			return $return_value;	

		}


	}

?>