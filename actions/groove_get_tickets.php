<?php

/*
 * 
 *
 * Class that creates any table records inside infusionsoft (extends default_is_action)
 *
 * @param $conn          (db pdo conn) db connection - inherited from default_action
 * @param $action        (String) action name - inherited from default_action
 * @param $user          (User) contains the user object - inherited from default_action
 * @param $action_params (String) json encoded array containing action params - inherited from default_action
 * @param $isdk          (Infusionsoft api sdk) - inherited from default_is_action
 * @return 
 */

	class groove_get_tickets extends default_is_action{

		public function __construct($conn, $action, $user, $action_params, $isdk){

			$this->action_perm_level = 3;

			parent::__construct($conn, $action, $user, $action_params, $isdk);

		}

		/*
		* function run
		*
		* runs the is action
		*
		* @param none
		* @return (Array) Returns either an error array or a success array
		*/

		public function run(){

			//add the action in the queue
			$this->queue->write();

			//user isn't allowed to run this action and it returns an error array
			if($this->allow != 1){

				$return_value = array(
					"message" => "401 - You don't have permission to run this action!",
					"status"  => "401 Unauthorized"
				);

				//send the run result to the queue object as a failed action
				$this->queue->add_result($return_value, false);	

			}else{

				$params       = json_decode($this->action_params);
				$return_value = array();

				if(!isset($params->email) ){

					$return_value = array(
						"message" => "400 - Invalid params1",
						"status"  => "400 Bad Request1"
					);

					//send the run result to the queue object as a failed action
					$this->queue->add_result($return_value, false);	

				}else{

					$agents  = $this->get_groove_agents();
					$tickets = $this->get_groove_tickets_by_email($params->email);

					$good_tickets = array();

					if($tickets){

						foreach($tickets->tickets as $ticket){

						// echo "<pre>";
						// 	print_r($ticket);
						// echo "</pre>";
							
							if($ticket->state != "closed" && $ticket->state != "spam"){

								$ticket_array          = array();
								$ticket_array['title'] = $ticket->title;
								$ticket_array['href']  = $ticket->href;

								$assignee = "none";

								if(isset($ticket->links->assignee->href) && isset($agents[$ticket->links->assignee->href])){

									$assignee = $agents[$ticket->links->assignee->href];

								}

								$ticket_array['assignee'] = $assignee;

								$created_timestamp = "";
								$created_date      = str_replace("T", "", $ticket->created_at);
								$created_date      = str_replace("Z", "", $created_date);
								$created_timestamp = DateTime::createFromFormat('Y-m-dH:i:s', $created_date)->getTimestamp();

								$updated_timestamp = "";
								$updated_date      = str_replace("T", "", $ticket->updated_at);
								$updated_date      = str_replace("Z", "", $updated_date);
								$updated_timestamp = DateTime::createFromFormat('Y-m-dH:i:s', $updated_date)->getTimestamp();

								$ticket_array['created'] = $created_timestamp;
								$ticket_array['updated'] = $updated_timestamp;
								$ticket_array['state']   = $ticket->state;

								$good_tickets[] = $ticket_array;

							}

						}

					}

					if(count($good_tickets) > 0){

						$return_value['tickets'] = $good_tickets;

					}else{

						$return_value['tickets'] = "none";

					}

					

					//send the run result to the queue object as a successfull action
					$this->queue->add_result($return_value, true);			

				}

			}

			$return_value = json_encode($return_value);
			return $return_value;	

		}


	}

?>