<?php

/*
 * default_is_action
 *
 * Main infusionsoft action class (extends default_action)
 *
 * @param $conn          (db pdo conn) db connection - inherited from default_action
 * @param $action        (String) action name - inherited from default_action
 * @param $user          (User) contains the user object - inherited from default_action
 * @param $action_params (String) json encoded array containing action params - inherited from default_action
 * @param $isdk          (Infusionsoft api sdk)
 * @return (default_is_action)
 */

	class default_is_action extends default_action{

		public $isdk;

		public function __construct($conn, $action, $user, $action_params, $isdk){

			$this->isdk = $isdk;

			parent::__construct($conn, $action, $user, $action_params);

		}

	}

?>