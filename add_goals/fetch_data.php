<?php
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	include_once('config.php');

	$order_data = array();

	$continue = true;
	$page     = 0;

	while( $continue ){

		$returnFields = array(
			'Id',
			'ContactId', 
			'ProductId', 
			'LastBillDate', 
			'BillingAmt', 
			'CC1', 
			'AutoCharge');

	    $query = array(
	    	'ProductId' => "%"
	    );

	    $recurring_order_data = $isdk->dsQuery("RecurringOrder", 1000, $page, $query, $returnFields);
	    $page++;
	    if(count($recurring_order_data) < 1000){

	    	$continue = false;

	    }

	    $order_data = array_merge($recurring_order_data, $order_data);

	}

    // echo "<pre>";
    // 	print_r($order_data);
    // echo "</pre>";

    //

    $insert_string = "INSERT INTO orders(`ContactId`, `ProductId`, `LastBillDate`, `BillingAmt`, `AutoCharge`) VALUES ";
    $count = 0;

    foreach( $order_data as $order ){

    	if($count > 0){

    		$insert_string .= " , ";

    	}

    	$date = '';
    	if(isset($order['LastBillDate'])){
    		$date = DateTime::createFromFormat('YmdH:i:s', str_replace("T","",$order['LastBillDate']))->format('Y-m-d H:i');
    	}

    	$insert_string .= "(";
    	$insert_string .= "'" . $order['ContactId'] . "'";
    	$insert_string .= " , '" . $order['ProductId'] . "'";
    	$insert_string .= " , '" . $date . "'";
    	$insert_string .= " , '" . $order['BillingAmt'] . "'";
    	$insert_string .= " , '" . $order['AutoCharge'] . "'";
    	$insert_string .= ")";

		$count++;

    }

    echo $insert_string;
    mysql_query($insert_string);
    echo mysql_error();

?>