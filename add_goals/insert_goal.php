<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

	// $username = "root";
	// $password = "root";

	// $conn = new PDO('mysql:host=localhost;dbname=rucksack', $username, $password);

	include_once("../config/config.php");

	$integration_name = $_POST['integration_name'];
	$callname         = $_POST['callname'];	
	$name             = $_POST['name'];
	$category         = $_POST['category'];

	// $integration_name = "a test";
	// $callname         = "a test2";

    // $query = "UPDATE macanta_install_records SET last_step = 7, last_modified = :last_modified WHERE _macantaInstallHash=:install_hash";

    // $statement = $this->conn->prepare($query);

    // $statement->execute(
    //     array(":install_hash" => $data['hash'], ":last_modified" => date("Y-m-d H:i"))
    // );

	$stmt = $conn->prepare("INSERT INTO api_goal(integration_name,callname,name,category) VALUES(:integration_name,:callname,:name,:category)");

	$stmt->execute(
		array(
			':integration_name' => $integration_name, 
			':callname'         => $callname, 
			':name'             => $name, 
			':category'         => $category
		)
	);

	$lastId = $conn->lastInsertId();

// print_r($conn->errorInfo());

	echo $lastId;


?>
