<?php
    $today    = date('Y-m-d');
    $products = file_get_contents("../products.txt");
    $products = explode("\n", $products);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>API goals manager</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  	<div role="navigation" class="navbar navbar-inverse"></div>
    <div role="main" class="container theme-showcase">
    	<div class="jumbotron">

    		<div class='main-content'>
    		<img src="img/macanta_logo.png" width="400px" style="display:block;width:400px;margin:-30px auto 30px auto"> 
	    		<h2 class="text-center" >API goals manager</h2>
	    		<div class="row row-centered">
	    			<div class="col-md-12 center-block merge-message">
	    			</div>
				 	<div class="col-md-8 col-centered">
					 	<div class="panel panel-default">
						 	<div class="panel-heading">
						    	<h3 class="panel-title">API goals manager<br/><small style="font-style:italic;font-size:14px">(just for managing goals) </small></h3>
						 	</div>
						 	<div class="panel-body">
<!-- 						    	<div class="input-group">
						    		<input type="text" class="form-control contact_id1">
							    	<span class="input-group-btn">
							        	<button class="btn btn-default find_contact" type="button" nr="1">Find</button>
							      	</span>
							    </div> -->

								<form class="form-horizontal">
									<div class="form-group">
										<label for="integration_name" class="col-sm-3 control-label">Integration name</label>
										<div class="col-sm-9">
											<input type="text" class="form-control" id="integration_name" placeholder="Integration name">
										</div>
									</div>
									<div class="form-group">
										<label for="callname" class="col-sm-3 control-label">Callname</label>
										<div class="col-sm-9">
											<input type="text" class="form-control" id="callname" placeholder="Callname">
										</div>
									</div>
									<div class="form-group">
										<label for="name" class="col-sm-3 control-label">Name</label>
										<div class="col-sm-9">
											<input type="text" class="form-control" id="name" placeholder="name">
										</div>
									</div>
									<div class="form-group">
										<label for="name" class="col-sm-3 control-label">Category</label>
										<div class="col-sm-9">
											<input type="text" class="form-control" id="category" placeholder="category">
										</div>
									</div>
<!-- 									<div class="form-group">
										<label for="prooducts" class="col-sm-3 control-label">Products</label>
										<div class="col-sm-9">
											<input type="text" class="form-control" id="prooducts" placeholder="prooducts">
										</div>
									</div> -->
									<div class="form-group">
										<div class="col-sm-12">
											<button class="btn btn-primary" type="button" style="display:block;margin:0 auto;width:100%" id="do_post">Add goal</button>
										</div>
									</div>
									<hr>
<!-- 									<div class="form-group">
										<div class="col-sm-12">
											<strong>Products:</strong>
											<div id="products_div" style="margin:20px 0">
											<?php

												//$i = 0;
												//foreach($products as $product){

													//$i++;
													//echo "<el class='prod$i'><span class='glyphicon glyphicon-remove glyphicon-remove-prod' aria-hidden='true' style='color:#C9302C;cursor:pointer' remove='prod$i' content='$product'></span> <span class='glyphicon glyphicon-pencil glyphicon-edit-prod' aria-hidden='true' style='color:#333;cursor:pointer' edit='prod$i'></span> <span class='product_p'>$product</span><br></el>";

												//}

											?>
											</div>
											<input class="form-control" type="text" placeholder="Product name" style="width:70%;margin-right:30px;display:block;float:left" id="product_input"> <button class="btn btn-primary add_product" type="button" style="display:block;float:left">Add</button>
											<div style="clear:both"></div>
											<br><button class="btn btn-success save_products" type="button" style="display:block;margin:10px auto;width:100%">Save changes</button>
										</div>
									</div> -->
									<hr>
								</form>

								<div class='post_result'>
								</div>
								
								<div class='api_goals_table'>
									<table class="table table-hover">
								      <thead>
								        <tr>
								          <th>Integration name</th>
								          <th>Callname</th>
								          <th>Delete</th>
								        </tr>
								      </thead>
								      <tbody>
								        <tr>
								          <th >1</th>
								          <td>Mark</td>
								          <td><span class="glyphicon glyphicon-remove" aria-hidden="true" style="color:#C9302C;cursor:pointer"></span></td>
								        </tr>
								        <tr>
								          <th >2</th>
								          <td>Jacob</td>
								          <td>Thornton</td>
								        </tr>
								        <tr>
								          <th >3</th>
								          <td>Larry</td>
								          <td>the Bird</td>
								        </tr>
								      </tbody>
								    </table>
								</div>

							</div>
						</div>

				 	</div>

<!-- 				 	<div class="col-md-5">
					 	<div class="panel panel-default">
						 	<div class="panel-heading">
						    	<h3 class="panel-title">Contact 2<br/><small style="font-style:italic;font-size:14px">(this is the duplicate contact, to merge into Contact 1)</small></h3>
						 	</div>
						 	<div class="panel-body">
						    	<div class="input-group">
						    		<input type="text" class="form-control contact_id2">
							    	<span class="input-group-btn">
							        	<button class="btn btn-default find_contact" type="button" nr="2">Find</button>
							      	</span>
							    </div>
								<div class='contact_div2' style="margin-top:30px"></div>
							</div>
						</div>

				 	</div> -->

				 	<div class="col-md-12 center-block">
				 		<button type="button" class="btn btn-primary center-block merge_button hidden">Merge</button>
				 	</div>
				</div>

			</div>

    	</div>
    </div>

	<div class="modal fade" id="no_id" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-sm">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	        <h4 class="modal-title" id="myModalLabel">Error</h4>
	      </div>
	      <div class="modal-body">
	      	Please insert a contact id.
	      </div>
		  <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>

	<div class="modal fade" id="merge_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
	        <h4 class="modal-title" id="myModalLabel">Confirm</h4>
	      </div>
	      <div class="modal-body confirm_modal_content">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        <button type="button" class="btn btn-primary merge_modal_button">Merge</button>
	      </div>
	    </div>
	  </div>
	</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
  </body>
</html>