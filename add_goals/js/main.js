$(function() {

	$(".api_goals_table").html("<p class='text-center'>Please wait...fetching goals from db.</p>");

	$.post("get_goals.php", {}, function(response){
		var goals = jQuery.parseJSON(response);

		var goals_html = "<table class='table table-hover api_table'><thead><tr><th>Id</th><th>Name</th><th>Integration name</th><th>Callname</th><th>Category</th><th>Delete</th></tr></thead><tbody>";

		$.each( goals, function( key, goal ) {
		    //console.log(goal.callname);
		    goals_html += "<tr class='row_goal" +  goal.id  + "'><th>" + goal.id + "</th><th>" + goal.name + "</th><th>" + goal.integration_name + "</th><th>" + goal.callname + "</th><th>" + goal.category + "</th><th><span class='glyphicon glyphicon-remove remove-goal' aria-hidden='true' style='color:#C9302C;cursor:pointer' goal_id='" +  goal.id  + "'></span></th></tr>";

		});

		goals_html += "</tbody></tbody>";
		$(".api_goals_table").html(goals_html);
	})

	$("#do_post").on('click', function(){

		$integration_name = $("#integration_name").val();
		$callname         = $("#callname").val();
		$name             = $("#name").val();
		$category         = $("#category").val();

		$.post("insert_goal.php", {integration_name:$integration_name,callname:$callname,name:$name,category:$category}, function(response){

			$(".api_table tbody").append("<tr class='row_goal" +  response  + "' ><th>" + response + "</th><th>" + $name + "</th><th>" + $integration_name + "</th><th>" + $callname + "</th><th>" + $category + "</th><th><span class='glyphicon glyphicon-remove remove-goal' aria-hidden='true' style='color:#C9302C;cursor:pointer' goal_id='" +  response  + "'></span></th></tr>");

		})

	})
	

	$(document.body).on('click', '.save_products', function(){

		$products = []

		$(".product_p").each( function( index, element ){
		    $products.push($(this).html());
		});

		$.post("save_products.php", {products:$products}, function(response){

			console.log(response);

		})

	})

	$(document.body).on('click', '.add_product', function(){

		$product  = $("#product_input").val();
		$products = []

		$(".product_p").each( function( index, element ){
		    $products.push($(this).html());
		});

		console.log($.inArray($product, $products));

		if($product != "" && $.inArray($product, $products) == -1){

			$now  = jQuery.now();
			$html = "<el class='prod" + $now + "'><span class='glyphicon glyphicon-remove glyphicon-remove-prod' aria-hidden='true' style='color:#C9302C;cursor:pointer' remove='prod" + $now + "' content='" + $product + "'></span> <span class='glyphicon glyphicon-pencil glyphicon-edit-prod' aria-hidden='true' style='color:#333;cursor:pointer' edit='prod" + $now + "'></span> <span class='product_p'>" + $product + "</span><br></el>"
			$("#products_div").append($html);

		}

	})

	$(document.body).on('click', '.remove-goal', function(){

		var goal_id = $(this).attr("goal_id");

		//console.log(goal_id);

		$.post("delete_goal.php", {id:goal_id}, function(response){

			$(".row_goal" + goal_id).remove();

		})

	})

	$(document.body).on('click', '.edit_products', function(){

		$(".product_buttons_area").html('<button class="btn btn-primary save_products" type="button">Save</button> <button class="btn btn-warning cancel_products" type="button">Cancel</button>');

		$products = $(".product_p").html();
		$(".product_p").html("<input type='text' class='form-control' class='products_input' value='" + $products + "'/>");

	})

	$(document.body).on('click', '.glyphicon-remove-prod', function(){

		$product = $(this).attr("remove");
		$content = $(this).attr("content");

		var conf = confirm("Are you sure you want to remove the product '" + $content + "' ?");

		if(conf){

			$("." + $product).remove();

		}

	})

})