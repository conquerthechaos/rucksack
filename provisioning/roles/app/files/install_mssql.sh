#!/bin/sh
#sudo chown -R vagrant:vagrant ~/freetds-0.91/
FREETDSDEVDIR=$(find ~ -maxdepth 1 -type d -name "freetds*")
PHPDEVDIR=$(find ~ -maxdepth 1 -type d -name "php5*")
PHPLIBDIR=$(find /usr/lib/php5/ -maxdepth 1 -type d -name "2*")
cd $FREETDSDEVDIR
./configure
make
mkdir ./lib
cp ./src/dblib/.libs/libsybdb.so ./lib/
cd $PHPDEVDIR/ext/mssql
phpize
./configure --with-mssql=$FREETDSDEVDIR
make
cd modules
cp mssql.so $PHPLIBDIR/
chmod 644 $PHPLIBDIR/mssql.so

