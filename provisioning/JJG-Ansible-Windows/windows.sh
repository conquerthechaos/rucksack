#!/bin/bash
#
# Windows shell provisioner for Ansible playbooks, based on KSid's
# windows-vagrant-ansible: https://github.com/KSid/windows-vagrant-ansible
#
# @todo - Allow proxy configuration to be passed in via Vagrantfile config.
#
# @see README.md
# @author Jeff Geerling, 2014
# @version 1.0
#

# Uncomment if behind a proxy server.
# export {http,https,ftp}_proxy='http://username:password@proxy-host:80'

ANSIBLE_PLAYBOOK=$1
ANSIBLE_HOSTS=$2
TEMP_HOSTS="/tmp/ansible_hosts"
DEF_PATH="/var/www/zuora-integrate"

if [ ! -f $DEF_PATH/$ANSIBLE_PLAYBOOK ]; then
  echo "Cannot find Ansible playbook."
  exit 1
fi

if [ ! -f $DEF_PATH/$ANSIBLE_HOSTS ]; then
  echo "Cannot find Ansible hosts."
  exit 2
fi

# Install Ansible and its dependencies if it's not installed already.
if [ ! -f /usr/bin/ansible ]; then
  echo "Installing Ansible dependencies"
  sudo apt-get install python python-dev -q -y
  echo "Installing pip via easy_install."
  wget http://peak.telecommunity.com/dist/ez_setup.py
  sudo python ez_setup.py && rm -f ez_setup.py
  sudo easy_install pip
  # Make sure setuptools are installed crrectly.
  sudo pip install setuptools --no-use-wheel --upgrade
  echo "Installing required python modules."
  sudo pip install paramiko pyyaml jinja2 markupsafe
  echo "Installing Ansible."
  sudo pip install ansible
fi

cp ${DEF_PATH}/${ANSIBLE_HOSTS} ${TEMP_HOSTS} && chmod -x ${TEMP_HOSTS}
echo "Running Ansible provisioner defined in Vagrantfile."
ansible-playbook ${DEF_PATH}/${ANSIBLE_PLAYBOOK} --inventory-file=${TEMP_HOSTS} --extra-vars "is_windows=true groups=development" --connection=local
rm ${TEMP_HOSTS}
