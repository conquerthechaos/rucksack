<?php

	include_once('config/config.php');

	$Rucksack = null;
	$headers  = apache_request_headers();
	$ip_addr = array("12");

	$stmt = $conn->prepare("
		INSERT INTO calls(header,request,at,server) 
		VALUES(:header,:request,:at,:server)
	");

	$stmt->execute(
		array(
			':header'  => json_encode($headers), 
			':request' => json_encode($_REQUEST),
			':at'      => date("Y-m-d H:i:s"),	
			':server'  => json_encode($_SERVER)
		)
	);

	if( in_array($_SERVER["REMOTE_ADDR"], $ip_addr)){

		if(!isset($_REQUEST['EventCategory']) ){

		 	header("HTTP/1.1 400 Bad Request");
		 	echo "400 - Invalid param";
		    exit;	

		}

		$Rucksack    = new Rucksack($conn, "zuora", "zuora_" . $_REQUEST['EventCategory'], json_encode($_REQUEST), $infusionsoft);

	}else{


		if(!isset($_POST['user']) || !isset($_POST['action']) || !isset($_POST['action_details'])){

		 	header("HTTP/1.1 400 Bad Request");
		 	echo "400 - Invalid param";
		    exit;	

		}

		$Rucksack    = new Rucksack($conn, $_POST['user'], $_POST['action'], $_POST['action_details'], $infusionsoft);

	}

	$exec_result = $Rucksack->auth();

	if(isset($exec_result['status'])){

	 	header("HTTP/1.1 " . $exec_result['status']);
	 	echo $exec_result['message'];
	    exit;	

	}else{

		$exec_result = $Rucksack->execute();

		if(isset($exec_result['status'])){

		 	header("HTTP/1.1 " . $exec_result['status']);
		 	echo $exec_result['message'];
		    exit;		

		}else{

			header("Content-Type: text/plain");
			print_r($exec_result);
			exit;

		}

	}
?>
