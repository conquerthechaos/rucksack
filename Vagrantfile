# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'yaml'

current_dir  = File.dirname(File.expand_path(__FILE__))
configValues = YAML.load_file("#{current_dir}/vagrant_config.yml")
data         = configValues['vagrantfile-local']

VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|
  config.vm.provider "virtualbox" do |v|
        v.customize ["modifyvm", :id, "--memory", "2024"]
  end
  config.vm.box = "scotch/box"
  config.vm.network "private_network", ip: "#{data['vm']['network']['private_network']}"
  config.vm.network "forwarded_port", guest: 22, host: "#{data['vm']['network']['local_ssh_port']}"
  config.vm.hostname = "local.#{data['client']['project']['client_project']}.com"
  config.vm.synced_folder ".", "/vagrant", :mount_options => ["dmode=777", "fmode=666"]
  config.vm.synced_folder ".", "/var/www/#{data['client']['project']['client_project']}", :mount_options => ["dmode=777", "fmode=666"]
  require 'rbconfig'
  # Use rbconfig to determine if we're on a windows host or not.
  is_windows = (RbConfig::CONFIG['host_os'] =~ /mswin|mingw|cygwin/)
  if is_windows
    # Provisioning configuration for shell script.
	config.vm.provision "shell" do |sh|
	  sh.path = "provisioning/JJG-Ansible-Windows/windows.sh"
	  sh.args = "provisioning/playbook.yml provisioning/hosts"
	end
  else
    config.vm.provision "ansible" do |ansible|
      ansible.limit = "all"
      ansible.inventory_path = "provisioning/hosts"
      ansible.extra_vars = {
        groups: "development"
      }
      ansible.playbook = "provisioning/playbook.yml"
    end
  end
end
