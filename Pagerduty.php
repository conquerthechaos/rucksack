<?php

	class Pagerduty {

		public $service_key = "d0b1c87b77474c8c971a1d810a913bd1";
		public $api_key     = "BG9hkNJxo3M4snpSgRz1";
		public $endpoint    = "https://events.pagerduty.com/generic/2010-04-15/create_event.json";
		public $details;
		public $description;

		public function __construct($details,$description){

			$this->details     = $details;
			$this->description = $description;

		}

		public function post(){

			$payload = array(
				"service_key" => $this->service_key,
  				"event_type"  => "trigger",
  				"description" => $this->description,
  				"client"      => "zuora",
  				"details"     => $this->details
			);

			$session = curl_init();
			curl_setopt($session, CURLOPT_URL, $this->endpoint);
			curl_setopt($session, CURLOPT_POSTFIELDS, json_encode($payload));
			curl_setopt($session, CURLOPT_HTTPHEADER, array(
			    'Content-type'=> 'application/json'
			));
			curl_setopt($session, CURLOPT_RETURNTRANSFER, 1);
			$result = curl_exec($session);
			curl_close($session);

			return $result;

		}

	}

?>