<?php
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

include_once("config/cron.config.php");
$infusionsoft->setHttpClient(new \Infusionsoft\Http\CurlClient());
$StoredToken = file_get_contents(dirname(__FILE__) . '/temp/token.dat');
$infusionsoft->setToken(unserialize(trim($StoredToken)));
$infusionsoft->refreshAccessToken();
$new_token = serialize($infusionsoft->getToken());
file_put_contents(dirname(__FILE__) . '/temp/token.dat', $new_token);

echo "done";
exit(0);
?>
