<?php

/*
 * Rucksack
 *
 * Main action delegator class
 *
 * @param $conn          (db pdo conn) db connection
 * @param $user          (User) contains the user object
 * @param $action        (String) action name
 * @param $action_params (String) json encoded array containing action params
 * @param $isdk          (Infusionsoft api object) the Infusionsoft api object
 * @return (Rucksack)
 */

	class Rucksack{

		private $_user;
		private $_action;
		private $_action_params;
		private $_conn;
		private $_auth_user;
		private $_isdk;
		public  $mainQueueId;

		public function __construct($conn, $user, $action, $action_params, $isdk) {

			$this->_user          = $user;
			$this->_action        = $action;
			$this->_action_params = $action_params;
			$this->_conn          = $conn;
			$this->_isdk          = $isdk;
			$this->mainQueueId    = 0;

		}

		/*
		* function auth
		*
		* Checks to see if the user doing the action call exists
		*
		* @param none
		* @return (User || Array) Returns a user object if user is found else returns an array containing an error message 
		*/

		public function auth(){

			//check the db for the user
			$sqlSelect = 'SELECT * FROM `user` WHERE username = :username';
			$sth       = $this->_conn->prepare($sqlSelect);

			$sth->execute(array(':username' => $this->_user));
			$result = $sth->fetchAll();

			//user not found - returning error array
			if(!$result){

				$return_value = array(
					"message" => "401",
					"status"  => "401 Unauthorized"
				);

				return $return_value;

			}else{

				//user found -  returning user object
				$this->_auth_user = $result[0];
				return $result;

			}

		}

		public function setMainQueue($queue_id){

			$this->mainQueueId = $queue_id;

		}

		/*
		* function execute
		*
		* Executes the requested action
		*
		* @param none
		* @return (Array) Returns an array with either the success message or with the error message
		*/

		public function execute(){

			//convert action_params into an array
			$json_action_params = json_decode($this->_action_params);

			//json_encode failed - we didn't recieve a valid json string, returning an error array
			if(!$json_action_params){

				$return_value = array(
					"message" => "400 - Invalid action params",
					"status"  => "400 Bad Request"
				);

			}else{

				//requested action doesn't exist - returns an error array
				if(!file_exists ("actions/" . $this->_action . ".php")){

					$return_value = array(
						"message" => "400 - Invalid action",
						"status"  => "400 Bad Request"
					);

				}else{

					//action exists and running it
					include_once("actions/" . $this->_action . ".php");

					$action_obj   = new $this->_action($this->_conn, $this->_action, $this->_auth_user, $this->_action_params, $this->_isdk);
					$return_value = $action_obj->check_perm_level();	

					$action_obj->set_main_queue($this->mainQueueId);

					if($return_value == 1){
						//returns a successful message array
						$return_value = $action_obj->run();

					}

				}

			}

			return $return_value;

		}

	}

?>